package de.helmholtzschule;

import javax.swing.table.DefaultTableModel;
import java.io.Serializable;

public class Doctor extends Staff implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//--> attributes
	boolean busy;
	int skillLevel;
	int patientId;
	int maxPatient;
	int healthTreated;
	int treatedToNextLevel;
		//The following array contains the ID of every treated patient of the doctor. The length depends
		//on the skill level.
	int[] assignedPats;
	
	//--> constructor
	Doctor(int id){
		this.busy = false;
		this.id = id;
		this.skillLevel = 1;
		//this.wage = -1*0.5*this.skillLevel*1000;
		this.wage = -1*500+75*Math.pow(wage, wage);
		this.maxPatient = this.skillLevel;
		this.healthTreated=0;
		this.treatedToNextLevel=10;
		this.assignedPats = new int[skillLevel];
			this.assignedPats[0]=-1;
	}
	
	//--> methods

	public int getNextFreeSlot() {
		int i=-1;
		do {		
			i++;	
		} while(assignedPats[i]!=-1);							//seeks for NEXT available slot
		return i;
	}
	
	public void expandPatArray() {
		int[]help = assignedPats.clone();
		assignedPats = new int[skillLevel];
		int i = 0;
		while(i<=help.length-1) {
			assignedPats[i]=help[i];
			i++;
		}
		assignedPats[i]=-1;
	}
	
	public void resetPatArray(PatientList patList, DefaultTableModel patTable) {
		int i=0;
		while(i<=assignedPats.length-1) {
			int patId=assignedPats[i];
			try {
			patList.setTreated(patId, false);
			patList.setDoctor(patId, -1);
			patTable.setValueAt(false, patId, 2);
			patTable.setValueAt(null, patId, 3);
			}catch (Exception e) {
			}
			assignedPats[i] = -1;
			i++;
		}
	}
	
	public int getPatientAmount() {
		int amount=0;
		int i=0;
		while (i<=assignedPats.length-1) {
			if(assignedPats[i]!=-1) {
				amount++;
			}
			i++;
		}
		return amount;
	}
}
