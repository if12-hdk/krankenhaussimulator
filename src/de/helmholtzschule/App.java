package de.helmholtzschule;

import java.awt.Color;

import java.io.File;
import java.io.FileReader;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Arrays;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.table.DefaultTableModel;

public class App {
	
	//--> attributes
	
	private static App app;
	private JFrame frame;
	private JFrame menu;
	private boolean isTutorialEnVisible;
	private boolean isTutorialDeVisible;
	public DoctorList docList;
	public PatientList patList;
	public Money money;
	public Day date;
	private JLabel moneyBalance;
	private JLabel moneyAmmount;
	private JLabel dayView;
	private String savePath;
	
	//--> constructor
	
	App() {
		initMenu();
	}
	
	//--> methods
	
	public static void main(String[] args) {		
		app = new App();
	}
	
	public boolean isInputInteger(String input) {
		if(input.matches("[0-9]+")) {
			return true;
		} else {
			return false;
		}		
	}

	
	private void initMenu() {
		menu = new JFrame("Menu");
			menu.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
			menu.setSize(500, 600);
			
			menu.setIconImage(Toolkit.getDefaultToolkit().getImage(App.class.getResource("icon/icon.png")));
			
		JPanel pmenuleft = new JPanel();
			pmenuleft.setLayout(new GridLayout(4, 0, 0, 20));
		JButton startB = new JButton("Start Game");
			startB.addActionListener(actionEvent ->{
				onStartBClick();
			});
		JButton loadB = new JButton("Load Game");
			loadB.addActionListener(ActionEvent ->{
				onloadBClick();
			});
		JSplitPane tutorialSplit = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
		
		isTutorialEnVisible = false;
		isTutorialDeVisible = false;
		JSplitPane middleSplit = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
		JTextArea tutorialText = new JTextArea();
		
		JButton tutorialDeB = new JButton("Start Tutorial DE");
			tutorialDeB.addActionListener(ActionEvent ->{
				onTutorialDeBClick(pmenuleft,tutorialText,middleSplit);
			});
		JButton tutorialEnB = new JButton("Start Tutorial EN");
			tutorialEnB.addActionListener(ActionEvent ->{
				onTutorialEnBClick(pmenuleft,tutorialText,middleSplit);
			});
		JButton exitmenuB = new JButton("Exit");
			exitmenuB.addActionListener(ActionEvent->{
				onexitBClick();
			});
		
			tutorialSplit.setLeftComponent(tutorialEnB);
			tutorialSplit.setRightComponent(tutorialDeB);
			tutorialSplit.setDividerLocation(250);
			
			pmenuleft.add(startB);
			pmenuleft.add(loadB);
			pmenuleft.add(tutorialSplit);
			pmenuleft.add(exitmenuB);
			menu.add(pmenuleft);
			menu.setVisible(true);
	}
	
	private void onTutorialEnBClick(JPanel pmenuleft, JTextArea tutorialText, JSplitPane middleSplit) {
		if(isTutorialEnVisible) {
			//In case the tutorial is already visible nothing happens.
			middleSplit.setRightComponent(null);
			isTutorialEnVisible = false;
			menu.setSize(500, 600);
			tutorialText.setText(null);
		} else {
			if(isTutorialDeVisible) {
				tutorialText.setText(null);
				isTutorialDeVisible = false;
			}
			
			isTutorialEnVisible=true;
			JScrollPane tutorailScroll = new JScrollPane(tutorialText);
			BufferedReader tutorialReader = null;
			try {
				tutorialReader = new BufferedReader(new FileReader(new File("src/en/Tutorial.txt")));
				String line;
				Font tutFont = new Font("Verdana",Font.ITALIC,16);
				tutorialText.setFont(tutFont);
				
				while((line = tutorialReader.readLine())!=null) {
					tutorialText.setText(tutorialText.getText()+"\n"+line);
				}
				
				middleSplit.setLeftComponent(pmenuleft);
				middleSplit.setRightComponent(tutorailScroll);
				middleSplit.setDividerLocation(500);
				menu.add(middleSplit);
				menu.setSize(1300, 600);
			} catch (IOException e) {
				middleSplit.setLeftComponent(pmenuleft);
				middleSplit.setRightComponent(tutorailScroll);
				menu.add(middleSplit);
				e.printStackTrace();
			} finally {
				//Close the file reader.
				try {
					tutorialReader.close();
				} catch (IOException e2) {
					e2.printStackTrace();
				}
			}
		}
	}

	private void onTutorialDeBClick(JPanel pmenuleft, JTextArea tutorialText, JSplitPane middleSplit) {
		
		if(isTutorialDeVisible) {
			//In case the tutorial is already visible nothing happens.
			middleSplit.setRightComponent(null);
			isTutorialDeVisible = false;
			menu.setSize(500, 600);
			tutorialText.setText(null);
		} else {
			if(isTutorialEnVisible) {
				tutorialText.setText(null);
				isTutorialEnVisible = false;
			}
			
			isTutorialDeVisible=true;
			JScrollPane tutorailScroll = new JScrollPane(tutorialText);
			BufferedReader tutorialReader = null;
			try {
				tutorialReader = new BufferedReader(new FileReader(new File("src/de/TutorialDE.txt")));
				String line;
				Font tutFont = new Font("Verdana",Font.ITALIC,16);
				tutorialText.setFont(tutFont);
				
				while((line = tutorialReader.readLine())!=null) {
					tutorialText.setText(tutorialText.getText()+"\n"+line);
				}
				
				middleSplit.setLeftComponent(pmenuleft);
				middleSplit.setRightComponent(tutorailScroll);
				middleSplit.setDividerLocation(500);
				menu.add(middleSplit);
				menu.setSize(1300, 600);
			} catch (IOException e) {
				middleSplit.setLeftComponent(pmenuleft);
				middleSplit.setRightComponent(tutorailScroll);
				menu.add(middleSplit);
				e.printStackTrace();
			} finally {
				//Close the file reader.
				try {
					tutorialReader.close();
				} catch (IOException e2) {
					e2.printStackTrace();
				}
			}
		}
	}
	
	private void onStartBClick() {
		docList = new DoctorList();
		patList = new PatientList();
		money = new Money();
		date = new Day();
		String [] docTableTitles = new String[] {"Doctor - ID","Skill - Level","Busy","Patient - ID"};
		DefaultTableModel docTable = new DefaultTableModel(docTableTitles, 50);
		String [] patTableTitles = new String[] {"Patient - ID","Health - Status","Treated","Doctor - ID"};
		DefaultTableModel patTable = new DefaultTableModel(patTableTitles, 100);
		String [] eventLogTitle = new String[] {"Event - Log"};
		DefaultTableModel eventLog = new DefaultTableModel(eventLogTitle, 0);
		initWindow(docTable, patTable, eventLog);
		menu.dispose();
	}
	
	private void onloadBClick() {
		
		//Ask user for filename.
		JDialog loading = new JDialog(menu);
			loading.setLayout(new GridLayout(3, 1));
			loading.setSize(420,250);
		JLabel loadNotification = new JLabel("Please enter file name.",SwingConstants.CENTER);
	
		JTextField input = new JTextField();
		JSplitPane confirmCancel = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
		JButton confirmLoad = new JButton("OK");
			confirmLoad.addActionListener(ActionEvent->{
				onconfirmLoadClick(loading,input);
		});
		JButton cancelSave = new JButton("Cancel");
			cancelSave.addActionListener(ActionEvent->{
			onCloseDialogBClick(loading);
		});
			confirmCancel.setEnabled(false);
			confirmCancel.setRightComponent(cancelSave);
			confirmCancel.setLeftComponent(confirmLoad);
			confirmCancel.setDividerLocation(200);
			loading.add(loadNotification);
			loading.add(input);
			loading.add(confirmCancel);
			loading.setVisible(true);
	}
	
	private void onconfirmLoadClick(JDialog loading, JTextField input) {
		String [] docTableTitles = new String[] {"Doctor - ID","Skill - Level","Busy","Patient - ID"};
		DefaultTableModel docTable = new DefaultTableModel(docTableTitles, 50);
		String [] patTableTitles = new String[] {"Patient - ID","Health - Status","Treated","Doctor - ID"};
		DefaultTableModel patTable = new DefaultTableModel(patTableTitles, 100);
		String [] eventLogTitle = new String[] {"Event - Log"};
		DefaultTableModel eventLog = new DefaultTableModel(eventLogTitle, 0);
		
		try {
			savePath = "save/"+input.getText();
			//Load doctor list.
			try {
				FileInputStream fileIn = new FileInputStream(savePath+"/docList.txt");
				ObjectInputStream objectIn = new ObjectInputStream(fileIn);
				docList = (DoctorList) objectIn.readObject();
				objectIn.close();
					
				int i = 0;
				while(i<= docList.doctors.length-1) {
					if(docList.isIdOccupied(i)) {
						//Load ID.
						docTable.setValueAt(i, i, 0);
						//Load skill level.
						docTable.setValueAt(docList.getSkill(i), i, 1);
						//Load busy status.
						docTable.setValueAt(docList.getBusy(i), i, 2);
						//Load assignments.
						docTable.setValueAt(Arrays.toString(docList.doctors[i].assignedPats), i, 3);
					}
					i++;
				}
			} catch(Exception e) {
				e.printStackTrace();
			}
				
			//Load money.
			try {
				FileInputStream fileIn = new FileInputStream(savePath+"/money.txt");
				ObjectInputStream objectIn = new ObjectInputStream(fileIn);
				money = (Money) objectIn.readObject();
				objectIn.close();
				} catch(Exception e) {
					e.printStackTrace();
				}
				
			//Load patient list.
			try {
				FileInputStream fileIn = new FileInputStream(savePath+"/patList.txt");
				ObjectInputStream objectIn = new ObjectInputStream(fileIn);
				patList = (PatientList) objectIn.readObject();
				objectIn.close();
						
				int i = 0;
				while(i<=patList.patients.length-1) {
					if(patList.isIdOccupied(i)) {
						//Create Timer.
						patList.loadPatient(patTable, eventLog, docList, docTable, patList.patients[i], money, app);
						//Load ID.
						patTable.setValueAt(i, i, 0);
						//Load health status.
						patTable.setValueAt(patList.patients[i].healthStatus, i, 1);
						//Load treated status.
						patTable.setValueAt(patList.getTreated(i), i, 2);
						//Load assigned doctor.
						if(patList.getTreated(i)) {
							patTable.setValueAt(patList.getDoctor(i), i, 3);
						} else {
							patTable.setValueAt(null, i, 3);
						}
					}
					i++;
				}
			} catch(Exception e) {
				e.printStackTrace();
			}
			
			//Load day.
			try {
				FileInputStream fileIn = new FileInputStream(savePath+"/day.txt");
				ObjectInputStream objectIn = new ObjectInputStream(fileIn);
				date = (Day) objectIn.readObject();
				objectIn.close();
			} catch(Exception e) {
				e.printStackTrace();
			}
				
			//Start game.
			initWindow(docTable,patTable,eventLog);
			menu.dispose();
		} catch(Exception e) {
			onCloseDialogBClick(loading);
		}
	}

	private void onBankruptcy() {
		frame.dispose();
		JFrame endScreen = new JFrame("Game Over");
			endScreen.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
			endScreen.setSize(500, 600);
			endScreen.setIconImage(Toolkit.getDefaultToolkit().getImage(App.class.getResource("icon/icon.png")));
			
		JPanel endPanel = new JPanel();	
			endPanel.setLayout(new GridLayout(4, 1, 0, 25));
		JLabel loseText = new JLabel("You lost the game by going bankrupt!",SwingConstants.CENTER);
		Font loseFont = new Font("Verdana",Font.BOLD,16);
			loseText.setFont(loseFont);
		JButton againB = new JButton("Try Again");
			againB.addActionListener(ActionEvent ->{
				onagainBClick(endScreen);
			});
		JButton mainMenuB = new JButton("Main Menu");
			mainMenuB.addActionListener(ActionEvent->{
				onmainMenuBClick(endScreen);
			});
		JButton exitB = new JButton("Exit");
			exitB.addActionListener(ActionEvent->{
				onexitBClick();
			});
		endPanel.add(loseText);
		endPanel.add(againB);
		endPanel.add(mainMenuB);
		endPanel.add(exitB);
		endScreen.add(endPanel);
		endScreen.setVisible(true);
	}
	
	private void onagainBClick(JFrame bankruptcy) {
		bankruptcy.dispose();
		//Reset all data.
		String [] docTableTitles = new String[] {"Doctor - ID","Skill - Level","Busy","Patient - ID"};
		DefaultTableModel docTable = new DefaultTableModel(docTableTitles, 50);
		String [] patTableTitles = new String[] {"Patient - ID","Health - Status","Treated","Doctor - ID"};
		DefaultTableModel patTable = new DefaultTableModel(patTableTitles, 100);
		String [] eventLogTitle = new String[] {"Event - Log"};
		DefaultTableModel eventLog = new DefaultTableModel(eventLogTitle, 0);
		docList = new DoctorList();
		patList = new PatientList();
		money = new Money();
		date = new Day();
		initWindow(docTable, patTable, eventLog);
	}

	private void onmainMenuBClick(JFrame bankruptcy) {
		bankruptcy.dispose();
		initMenu();
	}
	
	private void initWindow(DefaultTableModel docTableModel, DefaultTableModel patTableModel, DefaultTableModel eventLogModel) {
		Random rand = new Random();
			rand.nextInt(30000);
		
		//Creation of tables
		JTable docTable = new JTable(docTableModel);
			docTable.setEnabled(false);
			docTable.setShowGrid(false);
			docTable.setRowHeight(20);
		JTable patTable = new JTable(patTableModel);
			patTable.setEnabled(false);
			patTable.setShowGrid(false);
			patTable.setRowHeight(20);
		JTable eventLog = new JTable(eventLogModel);
			eventLog.setEnabled(false);
			eventLog.setRowHeight(25);
			
		//Timer for arrival of new patients
		Timer newPat = new Timer();
			newPat.schedule(new TimerTask() {
				@Override
				public void run() {
					try {
						patList.createNewPatient(patTableModel,docList,docTableModel,eventLogModel,money,app);
						eventLogModel.insertRow(0, new Object[] {"A patient arrived and needs medical attention."});
					} catch(Exception e) {
						eventLogModel.insertRow(0, new Object[] {"A patient has been dismissed!"});
					}
				}
			}, rand.nextInt(5000), 5000+rand.nextInt(10000));			
		
		//Timer for the date	
		Timer day = new Timer();
			day.schedule(new TimerTask() {
				@Override
				public void run() {
					date.setDayCount();
					money.setDailyCharge();
					setMoneyAmmountLabel();
					setDayView();
					eventLogModel.insertRow(0, new Object[] {"A new day has begun. The daily charge has been taken into account."});
					if(money.getCredit()<=0) {
						onBankruptcy();
						day.cancel();
						newPat.cancel();
					}
				}
			},date.dayDuration, date.dayDuration);
						
		//Implementation of GUI
		
		frame = new JFrame("Adventure Hospital");
			frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
			frame.setSize(1000,750);
			frame.setIconImage(Toolkit.getDefaultToolkit().getImage(App.class.getResource("icon/icon.png")));
			
		JPanel pLeft = new JPanel();														
			pLeft.setBackground(Color.white);
			pLeft.setBorder(BorderFactory.createLineBorder(Color.white, 20));			
			pLeft.setLayout(new GridLayout(7,0,0,20));
		Dimension pLeftMin = new Dimension(300, 0);
			pLeft.setMinimumSize(pLeftMin);
			pLeft.setSize(pLeftMin);						
		JSplitPane centralSplit = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
		
			JButton addDoctorB = new JButton("Hire Doctor");
				addDoctorB.setVisible(true);
				addDoctorB.setBackground(Color.green);
				addDoctorB.addActionListener(actionEvent ->{
					onaddDoctorBClick(docList,docTableModel,eventLogModel,money);
				});
			pLeft.add(addDoctorB);
		
			JButton deleteDoctorB = new JButton("Fire Doctor");
				deleteDoctorB.setVisible(true);
				deleteDoctorB.setBackground(Color.orange);
				deleteDoctorB.addActionListener(actionEvent ->{
					ondeleteDoctorBClick(docList,patList,docTableModel,eventLogModel,patTableModel);
				});		
			pLeft.add(deleteDoctorB);
		
			JButton assignDoctorB = new JButton("Assign Doctor");
				assignDoctorB.setVisible(true);
				assignDoctorB.setBackground(Color.cyan);
				assignDoctorB.addActionListener(actionEvent ->{
					onassignDoctorBClick(docList,docTableModel,patList,patTableModel);
				});			
			pLeft.add(assignDoctorB);		
		
			JSplitPane moneyView = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
				moneyView.setEnabled(true);
				moneyView.setDividerLocation(120);
				
			moneyBalance = new JLabel("Balance: "+Double.toString(money.getBalanceSheet()), SwingConstants.CENTER);
			moneyAmmount = new JLabel("Credits: "+Double.toString(money.getCredit()), SwingConstants.CENTER);
				moneyView.setLeftComponent(moneyBalance);
				moneyView.setRightComponent(moneyAmmount);
				//moneyView.setLeftComponent(Money.moneyBalance);
				//moneyView.setRightComponent(Money.moneyAmmount);
			pLeft.add(moneyView);
			
			dayView = new JLabel("Day "+date.getDayCount(), SwingConstants.CENTER);
			pLeft.add(dayView);			
			
			JButton saveB = new JButton("Save Game");
				saveB.addActionListener(actionEvent ->{
					
					JDialog saving = new JDialog(frame);
						saving.setLayout(new GridLayout(3, 1));
						saving.setSize(420,250);
					JLabel saveNotification = new JLabel("Please enter file name.",SwingConstants.CENTER);
					
					JTextField input = new JTextField();
					JSplitPane confirmCancel = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
					JButton confirmSave = new JButton("OK");
						confirmSave.addActionListener(ActionEvent->{
							onconfirmSaveClick(saving,input);
						});
					JButton cancelSave = new JButton("Cancel");
						cancelSave.addActionListener(ActionEvent->{
							onCloseDialogBClick(saving);
						});
						confirmCancel.setEnabled(false);
						confirmCancel.setRightComponent(cancelSave);
						confirmCancel.setLeftComponent(confirmSave);
						confirmCancel.setDividerLocation(200);
					saving.add(saveNotification);
					saving.add(input);
					saving.add(confirmCancel);
					saving.setVisible(true);
					
				});
			pLeft.add(saveB);
			
			JButton exitB= new JButton("Exit");
				exitB.setVisible(true);
				exitB.setBackground(Color.red);
				exitB.addActionListener(actionEvent ->{
					onexitBClick();
				});
			pLeft.add(exitB);			

		JSplitPane rightHorizSplit = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
			rightHorizSplit.setDividerLocation(450);			
		JScrollPane pTopRightLeft = new JScrollPane(docTable);
			pTopRightLeft.setBorder(BorderFactory.createLineBorder(Color.gray, 10));
		JScrollPane pTopRightRight = new JScrollPane(patTable);
			pTopRightRight.setBorder(BorderFactory.createLineBorder(Color.gray, 10));		
		JScrollPane pBottomRight = new JScrollPane(eventLog);
			pBottomRight.setBorder(BorderFactory.createLineBorder(Color.pink,20));
		JSplitPane topRightSplit = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);		
		Dimension topRightMin = new Dimension(328,0);
			pTopRightLeft.setMinimumSize(topRightMin);
			pTopRightRight.setMinimumSize(topRightMin);			
			topRightSplit.setLeftComponent(pTopRightLeft);
			topRightSplit.setRightComponent(pTopRightRight);		
			rightHorizSplit.setLeftComponent(topRightSplit);
			rightHorizSplit.setRightComponent(pBottomRight);		
			centralSplit.setLeftComponent(pLeft);
			centralSplit.setRightComponent(rightHorizSplit);
		frame.add(centralSplit);
		frame.setVisible(true);
	}
	
	private void onconfirmSaveClick(JDialog saving, JTextField input) {
		try {
			String userfile = input.getText();
			savePath = "save/"+userfile;
			File directory = new File(savePath);
				directory.mkdir();
			//savePath = savePath + ".txt";
			savePatList(patList);
			saveDocList(docList);
			saveMoney(money);
			saveDay(date);
			onCloseDialogBClick(saving);
		} catch(Exception e) {
			onCloseDialogBClick(saving);
		}
	}

	public void setMoneyAmmountLabel() {
		moneyAmmount.setText("Credits: "+money.getCredit());
	}
	
	public void setMoneyBalanceLabel() {
		moneyBalance.setText("Balance: "+ Double.toString(money.getBalanceSheet()));
	}
	
	private void setDayView() {
		dayView.setText("Day "+date.getDayCount());
	}
	
	private void onassignDoctorBClick(DoctorList docList, DefaultTableModel docTable, PatientList patList, DefaultTableModel patTable) {
		JDialog assignDoc = new JDialog(frame, "Assign Doctor", true);
			assignDoc.setLocationRelativeTo(frame);
			assignDoc.setResizable(false);
			assignDoc.setSize(700, 600);
			assignDoc.setLayout(new GridLayout(3, 2));
			
	//--> components			
		JTable doc = new JTable(docTable);
			doc.setShowGrid(false);
			doc.setEnabled(false);
			doc.setRowHeight(20);
		JTable pat = new JTable(patTable);
			pat.setShowGrid(false);
			pat.setEnabled(false);
			pat.setRowHeight(20);
		JScrollPane scrollDoc = new JScrollPane(doc);
		JScrollPane scrollPat = new JScrollPane(pat);
		JTextField userDoc = new JTextField("Enter Doctor ID.");
			userDoc.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY, 25));
			userDoc.addFocusListener(new java.awt.event.FocusAdapter() {
				public void focusGained(java.awt.event.FocusEvent evt) {
					ontextFieldEdit(userDoc);
				}
			});					
		JTextField userPat = new JTextField("Enter Patient ID.");
			userPat.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY, 25));
			userPat.addFocusListener(new java.awt.event.FocusAdapter() {
				public void focusGained(java.awt.event.FocusEvent evt) {
					ontextFieldEdit(userPat);
				}
			});	

		
		JButton confirmB = new JButton("OK");
			confirmB.setBorder(BorderFactory.createLineBorder(Color.white, 25));
			confirmB.addActionListener(actionEvent ->{
				onassignDoctorConfirmBClick(assignDoc, docList, docTable, patList, patTable, userDoc, userPat);
			});
		
		JButton resetB = new JButton("Reset assigned patients");
			resetB.setBorder(BorderFactory.createLineBorder(Color.white, 15));
			resetB.setBackground(Color.red);
			resetB.addActionListener(actionEvent ->{
				onresetBClick(assignDoc,docList, docTable, patList, patTable, userDoc, userPat);
			});
		JButton cancelB = new JButton("Cancel");
			cancelB.setBorder(BorderFactory.createLineBorder(Color.white, 15));
			cancelB.addActionListener(actionEvent -> {
				onCloseDialogBClick(assignDoc);
			});
		JSplitPane resetCancel = new JSplitPane(JSplitPane.VERTICAL_SPLIT, false, resetB, cancelB);
			resetCancel.setEnabled(false);
			resetCancel.setDividerLocation(75);
			
	//--> add components to view
		assignDoc.add(scrollDoc);
		assignDoc.add(scrollPat);
		assignDoc.add(userDoc);
		assignDoc.add(userPat);
		assignDoc.add(confirmB);
		assignDoc.add(resetCancel);
		
		assignDoc.setVisible(true);
	}

	private void onresetBClick(JDialog dialog,DoctorList docList, DefaultTableModel docTable, PatientList patList, DefaultTableModel patTable, JTextField docInput, JTextField patInput) {
				
		try {
			//Checks if the input is an integer.
			int docId = Integer.parseInt(docInput.getText());
			
			try {
				docList.doctors[docId].resetPatArray(patList,patTable);
				docList.setBusy(docId, false);
				//Refresh patient array.
				docTable.setValueAt(Arrays.toString(docList.doctors[docId].assignedPats), docId, 3);
				//Refresh busy-status.
				docTable.setValueAt(false, docId, 2);
			} catch (Exception e) {
				onCloseDialogBClick(dialog);
			}
			
		} catch (Exception e) {			
			JDialog errorNoInt = new JDialog(dialog, "Error Integer", true);
				errorNoInt.setResizable(false);
				errorNoInt.setSize(200, 100);
				errorNoInt.setLayout(new GridLayout(2,1));
				errorNoInt.setLocationRelativeTo(dialog);
			JLabel errorNoIntMessage = new JLabel("Please enter an integer.",SwingConstants.CENTER);
			JButton errorNoIntConfirmB = new JButton("OK");
				errorNoIntConfirmB.addActionListener(actionEvent ->{
					onCloseDialogBClick(errorNoInt);
				});
				errorNoInt.add(errorNoIntMessage);
				errorNoInt.add(errorNoIntConfirmB);
				errorNoInt.setVisible(true);
			}
		docInput.setText(null);
	}

	private void onassignDoctorConfirmBClick(JDialog dialog, DoctorList docList, DefaultTableModel docTable, PatientList patList, DefaultTableModel patTable, JTextField docInput, JTextField patInput) {
		//Test if the input is an integer.
		try {
			int docId = Integer.parseInt(docInput.getText());
			int patId = Integer.parseInt(patInput.getText());
		
			//If the doctor and patient exist...
			if(docList.isIdOccupied(docId)&&patList.isIdOccupied(patId)) {
				
				//If the patient is already treated...
				if(patList.getTreated(patId)) {
					//ID of the originally treating doctor.
					int docOcc = patList.getDoctor(patId);
					//Try to assign the patient to the new doctor.
					try {
						//Assign the patient to the new doctor.
						//At this point the exception is thrown if the patient array is full.
						docList.setPatient(patId, docId);
						//Refresh assigned patients of new doctor.
						docTable.setValueAt(Arrays.toString(docList.doctors[docId].assignedPats), docId, 3);
						
						//Check the busy-status of the doctor who received the assignment.
						if(docList.doctors[docId].getPatientAmount()==1) {
							docList.setBusy(docId, true);
							docTable.setValueAt(true, docId, 2);
						}
						//Assign the (new) doctor to the patient.
						patList.setDoctor(patId, docId);
						//Refresh the treating-doctor value in the table.
						patTable.setValueAt(patList.getDoctor(patId), patId, 3);
						
						//Remove patient from the patient array of the initial doctor.
						docList.resetPatient(docOcc, patId);
						//Refresh the assignments of the initial doctor.
						docTable.setValueAt(Arrays.toString(docList.doctors[docOcc].assignedPats), docOcc, 3);
						
						//Check the busy-status of the initial doctor.
						if(docList.doctors[docOcc].getPatientAmount()==0) {
							docList.setBusy(docOcc, false);
							docTable.setValueAt(false, docOcc, 2);
						}
					
					//If the doctor who is intended to receive the assignment already fully occupied,
					//a notification-dialog appears.
					} catch (Exception e) {
						JDialog errorFullOcc = new JDialog(dialog, "Error Occupation", true);
							errorFullOcc.setResizable(false);
							errorFullOcc.setSize(200, 100);
							errorFullOcc.setLayout(new GridLayout(2, 1));
							errorFullOcc.setLocationRelativeTo(dialog);
							JLabel errorNoIDMessage = new JLabel("Doctor is already fully occupied.",SwingConstants.CENTER);
							JButton errorNoIDConfirmB = new JButton("OK");
							errorNoIDConfirmB.addActionListener(actionEvent ->{
								onCloseDialogBClick(errorFullOcc);
							});	
							errorFullOcc.add(errorNoIDMessage);
							errorFullOcc.add(errorNoIDConfirmB);
							errorFullOcc.setVisible(true);
						}					
					
				//If the patient is not treated yet...	
				} else {
					//Try to assign the patient to the doctor.
					try {
						//Assign the patient to the doctor.
						//At this point the exception is thrown if the patient array is full.
						docList.setPatient(patId, docId);
						//Refresh the assigned patients array.
						docTable.setValueAt(Arrays.toString(docList.doctors[docId].assignedPats), docId, 3);
						
						//Check the busy-status of the doctor.
						if(docList.doctors[docId].getPatientAmount()==1) {
							docList.setBusy(docId, true);
							docTable.setValueAt(true, docId, 2);
						}
					
						//Set the treatment-status true.
						patList.setTreated(patId, true);
						//Assign doctor to patient.
						patList.setDoctor(patId, docId);
						//Refresh the treatment-status.
						patTable.setValueAt(true, patId, 2);
						//Refresh the ID of the treating doctor.
						patTable.setValueAt(patList.getDoctor(patId), patId, 3);
						
					//If the doctor who is intended to receive the assignment already fully occupied,
					//a notification-dialog appears.
					} catch (Exception e) {
						JDialog errorFullOcc = new JDialog(dialog, "Error Occupation", true);
							errorFullOcc.setResizable(false);
							errorFullOcc.setSize(200, 100);
							errorFullOcc.setLayout(new GridLayout(2, 1));
							errorFullOcc.setLocationRelativeTo(dialog);
							JLabel errorNoIDMessage = new JLabel("Doctor is already fully occupied.",SwingConstants.CENTER);
							JButton errorNoIDConfirmB = new JButton("OK");
							errorNoIDConfirmB.addActionListener(actionEvent ->{
								onCloseDialogBClick(errorFullOcc);
							});	
							errorFullOcc.add(errorNoIDMessage);
							errorFullOcc.add(errorNoIDConfirmB);
							errorFullOcc.setVisible(true);
					}
				}
				
				patInput.setText(null);
				docInput.setText(null);
			
			//If the doctor/patient ID is not occupied, a notification-dialog appears.	
			} else {
				JDialog errorNoID = new JDialog(dialog, "Error ID", true);
					errorNoID.setResizable(false);
					errorNoID.setSize(200, 100);
					errorNoID.setLayout(new GridLayout(2, 1));
					errorNoID.setLocationRelativeTo(dialog);
				JLabel errorNoIDMessage = new JLabel("",SwingConstants.CENTER);
				
				if(docList.isIdOccupied(docId)) {
					errorNoIDMessage.setText("There is no patient with the ID "+patId+".");
				} else {
					errorNoIDMessage.setText("There is no doctor with the ID "+docId+".");
				}				
				
				JButton errorNoIDConfirmB = new JButton("OK");
					errorNoIDConfirmB.addActionListener(actionEvent ->{
						onCloseDialogBClick(errorNoID);
					});
				
					errorNoID.add(errorNoIDMessage);
					errorNoID.add(errorNoIDConfirmB);
					errorNoID.setVisible(true);
			}

		//If the input is not an integer, a notification-dialog appears.
		} catch (Exception e) {
			JDialog errorNoInt = new JDialog(dialog, "Error Integer", true);
				errorNoInt.setResizable(false);
				errorNoInt.setSize(200, 100);
				errorNoInt.setLayout(new GridLayout(2,1));
				errorNoInt.setLocationRelativeTo(dialog);
			JLabel errorNoIntMessage = new JLabel("Please enter an integer.",SwingConstants.CENTER);
			JButton errorNoIntConfirmB = new JButton("OK");
				errorNoIntConfirmB.addActionListener(actionEvent ->{
					onCloseDialogBClick(errorNoInt);
				});
				errorNoInt.add(errorNoIntMessage);
				errorNoInt.add(errorNoIntConfirmB);
				errorNoInt.setVisible(true);
		}
	}

	private void ontextFieldEdit(JTextField userInput) {
		userInput.setText("");
	}

	private void ondeleteDoctorBClick(DoctorList docList,PatientList patList, DefaultTableModel tableModel, DefaultTableModel event, DefaultTableModel patTable) {			
		//Check if the doctor list is empty.
		if(docList.isListEmpty()) {
			JDialog errorNoDoc = new JDialog(frame, "Fire Doctor - Error", true);
				errorNoDoc.setLocationRelativeTo(frame);
				errorNoDoc.setResizable(false);
				errorNoDoc.setSize(400, 150);
				errorNoDoc.setLayout(new GridLayout(1, 2));
			//Notification label.
			JLabel errorNoDocMessage = new JLabel("There is no doctor to be fired!", SwingConstants.CENTER);
				errorNoDocMessage.setBorder(BorderFactory.createLineBorder(Color.white, 5));
			//Button to confirm error.
			JButton errorNoDocConfirmB = new JButton("OK");
				errorNoDocConfirmB.setBorder(BorderFactory.createLineBorder(Color.white, 20));
				errorNoDocConfirmB.addActionListener(actionEvent -> {
					onCloseDialogBClick(errorNoDoc);
				});
				errorNoDoc.add(errorNoDocMessage);
				errorNoDoc.add(errorNoDocConfirmB);
				errorNoDoc.setVisible(true);			
		//If the doctor list is filled..
		} else {
			JDialog fireDoc = new JDialog(frame,"Fire doctor",true);		
				fireDoc.setLocationRelativeTo(frame);
				fireDoc.setSize(400, 300);
				fireDoc.setResizable(false);
			JSplitPane fireDocCentralSplit = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
				fireDocCentralSplit.setDividerLocation(150);
				fireDocCentralSplit.setEnabled(false);
			JPanel pFireDocTop = new JPanel(new GridLayout(2, 1));	
			JPanel pFireDocBottom = new JPanel(new GridLayout(1, 2));
				pFireDocBottom.setBorder(BorderFactory.createLineBorder(Color.white, 30));
			//Which doctor will be fired?
			JLabel inputRequest = new JLabel("Please enter the ID.", SwingConstants.CENTER);
			JTextField userInput = new JTextField();
			
				pFireDocTop.add(inputRequest);
				pFireDocTop.add(userInput);
			
			JButton fireDocConfirmB = new JButton("OK");
				fireDocConfirmB.addActionListener(actionEvent -> {
					onfireDocConfirmBClick(docList,patList, fireDoc, userInput, tableModel, event, patTable);
				});
			
			JButton fireDocCancelB = new JButton("Cancel");
				fireDocCancelB.addActionListener(actionEvent -> {
					onCloseDialogBClick(fireDoc);
				});
			
				pFireDocBottom.add(fireDocConfirmB);
				pFireDocBottom.add(fireDocCancelB);
				fireDocCentralSplit.setRightComponent(pFireDocBottom);
				fireDocCentralSplit.setLeftComponent(pFireDocTop);	
				fireDoc.add(fireDocCentralSplit);
				fireDoc.setVisible(true);
		}
	}

	private void onfireDocConfirmBClick(DoctorList docList, PatientList patList, JDialog fireDoc, JTextField input, DefaultTableModel tableModel, DefaultTableModel event, DefaultTableModel patTable) {
		//Test if the input is an integer.
		try {
			int id = Integer.parseInt(input.getText());
				try {
					//Reset assignments and refresh the patient table.
					docList.doctors[id].resetPatArray(patList, patTable);
					//The doctor is removed from the list and table.
					docList.deleteDoctor(id, tableModel, app);
					//Close dialog.
					onCloseDialogBClick(fireDoc);
					//Player notification through event-log.
					event.insertRow(0,new Object[] {"Doctor ID: "+id+" has been fired."});
				//If the ID is not occupied..
				} catch (Exception e) {
					JDialog errorNoID = new JDialog(fireDoc, "Error ID", true);
						errorNoID.setResizable(false);
						errorNoID.setSize(200, 100);
						errorNoID.setLayout(new GridLayout(2, 1));
						errorNoID.setLocationRelativeTo(fireDoc);
					//Notification label.
					JLabel errorNoIDMessage = new JLabel("There is no doctor with the ID "+id+".",SwingConstants.CENTER);
					//Button to confirm error.
					JButton errorNoIDConfirmB = new JButton("OK");
						errorNoIDConfirmB.addActionListener(actionEvent ->{
							onCloseDialogBClick(errorNoID);
						});
						errorNoID.add(errorNoIDMessage);
						errorNoID.add(errorNoIDConfirmB);
						errorNoID.setVisible(true);
				}
		//If the input is not an integer..
		} catch (Exception e) {
			JDialog errorNoInt = new JDialog(fireDoc, "Error Integer", true);
				errorNoInt.setResizable(false);
				errorNoInt.setSize(200, 100);
				errorNoInt.setLayout(new GridLayout(2,1));
				errorNoInt.setLocationRelativeTo(fireDoc);
			//Notification label.
			JLabel errorNoIntMessage = new JLabel("Please enter an integer.",SwingConstants.CENTER);
			//Button to confirm error.
			JButton errorNoIntConfirmB = new JButton("OK");
				errorNoIntConfirmB.addActionListener(actionEvent ->{
					onCloseDialogBClick(errorNoInt);
				});
				errorNoInt.add(errorNoIntMessage);
				errorNoInt.add(errorNoIntConfirmB);
				errorNoInt.setVisible(true);
		}
	}

	private void onCloseDialogBClick(JDialog dialog) {	
		dialog.dispose();
	}

	private void onaddDoctorBClick(DoctorList docList, DefaultTableModel tableModel, DefaultTableModel event, Money money) {
	// This function creates a new doctor (array).
	// If the array is full, the user will be notified by the event log. 
		try {
			docList.createNewDoctor(tableModel, event, money, app);
		} catch (Exception e) {
			event.insertRow(0, new Object[] {"Another doctor can not be hired."});
		}		
	}

	private void saveDocList(Object list) {
		try {
			FileOutputStream fileOut = new FileOutputStream(savePath+"/docList.txt");
			//FileOutputStream fileOut = new FileOutputStream("save/docList.txt");
			ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
			objectOut.writeObject(list);
			objectOut.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void savePatList(Object list) {
		try {
			FileOutputStream fileOut = new FileOutputStream(savePath+"/patList.txt");
			//FileOutputStream fileOut = new FileOutputStream("save/patList.txt");
			ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
			objectOut.writeObject(list);
			objectOut.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void saveMoney(Object money) {
		try {
			FileOutputStream fileOut = new FileOutputStream(savePath+"/money.txt");
			//FileOutputStream fileOut = new FileOutputStream("save/money.txt");
			ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
			objectOut.writeObject(money);
			objectOut.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void saveDay(Object day) {
		try {
			FileOutputStream fileOut = new FileOutputStream(savePath+"/day.txt");
			//FileOutputStream fileOut = new FileOutputStream("save/day.txt");
			ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
			objectOut.writeObject(day);
			objectOut.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void onexitBClick() {
		System.exit(0);
	}
}