package de.helmholtzschule;

import java.io.Serializable;

public class Staff extends Person implements Serializable{
	private static final long serialVersionUID = 1L;
	//--> attributes
	
	public int id;
	public double wage;

	//--> methods
	
	public double getWage() {
		return this.wage;
	}
}