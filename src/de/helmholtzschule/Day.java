package de.helmholtzschule;

import java.io.Serializable;

public class Day implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//--> attributes
	
	int dayDuration = 40000;
	int dayCount;
	//public static JLabel dayView;
	
	//--> constructor
	
	Day(){
		this.dayCount=1;
		//dayView = new JLabel("Day "+Integer.toString(this.getDayCount()),SwingConstants.CENTER);
	}
	
	//--> methods
	
	public void setDayCount() {
		this.dayCount++;
		//this.dayView.setText("Day "+Integer.toString(this.getDayCount()));
	}
	public int getDayCount() {
		return this.dayCount;
	}
}
