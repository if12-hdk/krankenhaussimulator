package de.helmholtzschule;

import java.util.Arrays;
import java.util.Timer;
import java.util.TimerTask;
import java.io.Serializable;
import javax.swing.table.DefaultTableModel;

public class PatientList extends List implements Serializable{
	private static final long serialVersionUID = 1L;
	
	//--> attributes
	
	public Patient[] patients = new Patient[100];
	
	//--> methods
	
	private int getSize() {
		int size=0;
		int i=-1;
		do {
			i++;
			if(patients[i]!=null) {
				size++;
			}
		} while(i<patients.length-1);
		return size;
	}
	
	
	public boolean isSlotAvailable() {					//returns if the array contains free slots
		if(getSize()<50) {
			return true;									//true if ANY slot is available
		} else {
			return false;									//false if no slot is available
		}
	}
	
	
	public boolean isListEmpty() {						//returns if the array is empty
		if(getSize()==0) {
			return true;									//true if size equals 0
		} else {
			return false;									//false if size greater 0
		}
	}
	
	
	public boolean isIdOccupied(int i) {					//returns if a certain slot is occupied
		// If the given ID is out of the array's bounds, it definitely does not exist.
		// The attempt to check the array out of bounds throws an exception, which is caught, triggering
		// the function to return false.
		try {
			if(patients[i]!=null) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}
	
	
	public int getNextFreeSlot() {						//returns NEXT free slot in the array
		int i=-1;
		do {		
			i++;	
		} while(patients[i]!=null);							//seeks for NEXT available slot
		return i;
	}
	
	public void setTreated(int i, boolean value) {
		patients[i].treated = value;
	}
	
	public boolean getTreated(int i) {
		if(patients[i].treated) {
			return true;
		} else {
			return false;
		}
	}
	
	public void setDoctor(int pat, int doc) {
		patients[pat].doctorId = doc;
	}
	
	public int getDoctor(int i) {
		return patients[i].doctorId;
	}
	
	public void loadPatient(DefaultTableModel tableModel, DefaultTableModel eventLog, DoctorList docs, DefaultTableModel docTable, Patient patient, Money money, App app) {
		int id = patient.id;
			
		Timer health = new Timer();
		health.schedule(new TimerTask() {
			@Override
			public void run() {
				
				// i equals the patient's ID.
				int i=id;
				
				//If the patient is treated, his health status will increase.
				if(patients[i].treated) {
					int treatingDoctor = patients[i].doctorId;
					//The increase of the health status depends on the treating doctor's skill level.
					patients[i].healthStatus=patients[i].healthStatus + 2 * docs.getSkill(treatingDoctor);
					//The treating doctor's experience increases as well.
					docs.increaseTreated(treatingDoctor,docTable,money,app);
					//Refresh the health status.
					tableModel.setValueAt(patients[i].healthStatus, i, 1);
					
					//The patient has been successfully treated.
					if(patients[i].healthStatus>=100) {
						//The player receives an amount of money depending on the initial health status
						//of the patient arriving at the hospital.
						double revenue = 35*(100-patients[i].initStatus);
							money.setCredit(revenue);
							app.setMoneyAmmountLabel();
						//The assignment becomes invalid as the patient will disappear.
						docs.resetPatient(treatingDoctor,i);
						
						//The treating doctor becomes lazy if there are no other patients treated.
						if(docs.doctors[treatingDoctor].getPatientAmount()==0) {
							docs.setBusy(treatingDoctor, false);
							docTable.setValueAt(false, treatingDoctor, 2);
						}
							//The assignment vanishes from the doctor table.
							docTable.setValueAt(Arrays.toString(docs.doctors[treatingDoctor].assignedPats), treatingDoctor, 3);
							//The treated patient is removed from the table.
							deletePatient(i,tableModel);
							//The player receives a notification that the patient has been treated
							//successfully.
							eventLog.insertRow(0, new Object[] {"A patient has been successfully treated! Your revenue: "+revenue});
							//The belonging timer is purged as its no longer needed.
							health.cancel();
							health.purge();
					}
				} else {
				//If the patient is not treated, the health status will decrease.
					patients[i].healthStatus--;
					//Refresh the health status.
					tableModel.setValueAt(patients[i].healthStatus, i, 1);
					
					//In case the health status drops below zero, the patient dies.
					if(patients[i].healthStatus<0) {
						//The belonging timer is purged as its no longer needed.
						health.cancel();
						health.purge();						
						//The dead patient is removed from the table.
						deletePatient(id,tableModel);
						//-->> punishment for dead patient??			
						//The patient receives a notification that the patient has died.
						eventLog.insertRow(0, new Object[] {"A patient has not been treated and died. Penalty: 2000 Credits."});
						//The penalty for letting a patient die is 2000 money units.
						money.setCredit(-2000);
						app.setMoneyAmmountLabel();
					}
				}
			}	
		//The health status refreshes every 3 seconds.
		}, 0, 3000);
	
	}
	
	public void createNewPatient(DefaultTableModel tableModel, DoctorList docs, DefaultTableModel docTable, DefaultTableModel eventLog, Money money, App app) {						
		//Seek for the next available ID.
		int id = getNextFreeSlot();
		//Create new patient.
		patients[id] = new Patient(id);
		//Add the new patient to the table.
		tableModel.setValueAt(id, id, 0);
		tableModel.setValueAt(patients[id].healthStatus, id, 1);
		tableModel.setValueAt(patients[id].treated, id, 2);
		
		//Every new patient has an own timer refreshing the health status.
		Timer health = new Timer();
			health.schedule(new TimerTask() {
				@Override
				public void run() {
					
					// i equals the patient's ID.
					int i=id;
					
					//If the patient is treated, his health status will increase.
					if(patients[i].treated) {
						int treatingDoctor = patients[i].doctorId;
						//The increase of the health status depends on the treating doctor's skill level.
						patients[i].healthStatus=patients[i].healthStatus + 2 * docs.getSkill(treatingDoctor);
						//The treating doctor's experience increases as well.
						docs.increaseTreated(treatingDoctor,docTable,money,app);
						//Refresh the health status.
						tableModel.setValueAt(patients[i].healthStatus, i, 1);
						
						//The patient has been successfully treated.
						if(patients[i].healthStatus>=100) {
							//The player receives an amount of money depending on the initial health status
							//of the patient arriving at the hospital.
							double revenue = 35*(100-patients[i].initStatus);
								money.setCredit(revenue);
								app.setMoneyAmmountLabel();
							//The assignment becomes invalid as the patient will disappear.
							docs.resetPatient(treatingDoctor,i);
							
							//The treating doctor becomes lazy if there are no other patients treated.
							if(docs.doctors[treatingDoctor].getPatientAmount()==0) {
								docs.setBusy(treatingDoctor, false);
								docTable.setValueAt(false, treatingDoctor, 2);
							}
								//The assignment vanishes from the doctor table.
								docTable.setValueAt(Arrays.toString(docs.doctors[treatingDoctor].assignedPats), treatingDoctor, 3);
								//The treated patient is removed from the table.
								deletePatient(i,tableModel);
								//The player receives a notification that the patient has been treated
								//successfully.
								eventLog.insertRow(0, new Object[] {"A patient has been successfully treated! Your revenue: "+revenue});
								//The belonging timer is purged as its no longer needed.
								health.cancel();
								health.purge();
						}
					} else {
					//If the patient is not treated, the health status will decrease.
						patients[i].healthStatus--;
						//Refresh the health status.
						tableModel.setValueAt(patients[i].healthStatus, i, 1);
						
						//In case the health status drops below zero, the patient dies.
						if(patients[i].healthStatus<0) {
							//The belonging timer is purged as its no longer needed.
							health.cancel();
							health.purge();						
							//The dead patient is removed from the table.
							deletePatient(id,tableModel);
							//-->> punishment for dead patient??			
							//The patient receives a notification that the patient has died.
							eventLog.insertRow(0, new Object[] {"A patient has not been treated and died. Penalty: 2000 Credits."});
							//The penalty for letting a patient die is 2000 money units.
							money.setCredit(-2000);
							app.setMoneyAmmountLabel();
						}
					}
				}	
			//The health status refreshes every 3 seconds.
			}, 0, 3000);		
		}
	
	
	public void deletePatient(int i, DefaultTableModel tableModel) {
		//Reset the value in the array.
		patients[i]=null;
		//Remove the patient from the table.
		tableModel.setValueAt(null, i, 0);
		tableModel.setValueAt(null, i, 1);
		tableModel.setValueAt(null, i, 2);
		tableModel.setValueAt(null, i, 3);
	}

}
