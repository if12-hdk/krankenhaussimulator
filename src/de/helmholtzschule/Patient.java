package de.helmholtzschule;

import java.io.Serializable;
import java.util.Random;

public class Patient extends Person implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//--> attributes
	
	boolean treated;
	int healthStatus;
		//initial healthStatus
	int initStatus;
	int id;
	int doctorId;
	
	//--> constructor
	
	Patient(int id){
		this.treated = false;
		this.healthStatus = new Random().nextInt(99);
		this.id = id;
		this.initStatus = this.healthStatus;
		/*if(this.treated) {
			this.doctorId = docId;
		}*/
	}
	
}
