package de.helmholtzschule;

import java.util.Arrays;
import java.io.Serializable;
import javax.swing.table.DefaultTableModel;

public class DoctorList extends List implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//--> attributes
	public Doctor[] doctors = new Doctor[50];			//creates array of class Doctor -> length 50
	
	//--> methods
	
	public int getSize() {
		int size=0;
		int i=-1;
		do {
			i++;
			if(doctors[i]!=null) {
				size++;
			}
		} while(i<doctors.length-1);
		return size;
	}
		
	public boolean isListEmpty() {						//returns if the array is empty
		if(getSize()==0) {
			return true;									//true if size equals 0
		} else {
			return false;									//false if size greater 0
		}
	}
	
	public boolean isIdOccupied(int i) {					//returns if a certain slot is occupied
		// If the given ID is out of the array's bounds, it definitely does not exist.
		// The attempt to check the array out of bounds throws an exception, which is caught, triggering
		// the function to return false.
		try {
			if(doctors[i]!=null) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
	}
	
	public int getNextFreeSlot() {						//returns NEXT free slot in the array
		int i=-1;
		do {		
			i++;	
		} while(doctors[i]!=null);							//seeks for NEXT available slot
		return i;
	}
	
	public void setBusy(int id, boolean value) {
		doctors[id].busy = value;
	}
	public boolean getBusy(int id) {
		return doctors[id].busy;
	}
	
	public void setPatient(int pat, int doc) {
		doctors[doc].assignedPats[doctors[doc].getNextFreeSlot()]=pat;
	}
	
	public int getPatient(int i) {
		return doctors[i].patientId;
	}
	
	public int getSkill(int i) {
		return doctors[i].skillLevel;
	}
	
	public void increaseTreated(int id, DefaultTableModel docTable, Money money, App app) {
		doctors[id].healthTreated++;
		//The maximal level is 4.
		if(isNextLevelReached(id)&&doctors[id].skillLevel<4) {
			
			increaseSkill(id);
			setTreatedToNextLevel(id);
			refreshWage(id,money,app);
			docTable.setValueAt(doctors[id].skillLevel, id, 1);
			docTable.setValueAt(Arrays.toString(doctors[id].assignedPats), id, 3);
		}
	}
	
	private boolean isNextLevelReached(int id) {
		if(doctors[id].healthTreated>=doctors[id].treatedToNextLevel) {
			return true;
		} else {
			return false;
		}
	}
	
	private void increaseSkill(int id) {
		doctors[id].skillLevel++;
		doctors[id].maxPatient++;
		doctors[id].expandPatArray();
		resetHealthTreated(id);
	}	
	
	private void refreshWage(int id, Money money, App app) {
		
		//Erase influence of initial wage.
		money.setBalanceSheet((-1)*doctors[id].wage);
			
		doctors[id].wage=(-1)*(500+75*Math.pow(doctors[id].skillLevel, doctors[id].skillLevel));
		money.setBalanceSheet(doctors[id].wage);
		app.setMoneyBalanceLabel();
	}
	
	private void resetHealthTreated(int id) {
		doctors[id].healthTreated=0;
	}
	
	private void setTreatedToNextLevel(int id) {
		doctors[id].treatedToNextLevel = (int) Math.pow(5, (doctors[id].skillLevel));
	}
	
	public void resetPatient(int docId, int patId) {
		//out of patient array bounds
		int i=0;
		while(doctors[docId].assignedPats[i]!=patId) {
			i++;
		}
		doctors[docId].assignedPats[i]=-1;
	}
		
	public void createNewDoctor(DefaultTableModel tableModel, DefaultTableModel event, Money money, App app) {						//adds an object (doctor) to the array
		int id = getNextFreeSlot();
		doctors[id] = new Doctor(id);
		//Print ID.
		tableModel.setValueAt(id, id, 0);
		//Print default skill level.
		tableModel.setValueAt(1, id, 1);
		//Print (default) occupation status (false).
		tableModel.setValueAt(doctors[id].busy, id, 2);
		//Print (default) patient array [-1].
		tableModel.setValueAt(Arrays.toString(doctors[id].assignedPats), id, 3);
		//Print user notification in the event log.
		event.insertRow(0, new Object[] {"Doctor ID: "+id+", Skill: 1"+" hired.",null});
		//Refresh money balance.
		money.setBalanceSheet(doctors[id].getWage());
		app.setMoneyBalanceLabel();
	}
	
	public void deleteDoctor(int i, DefaultTableModel tableModel, App app) {
		
		app.money.setBalanceSheet(-1*doctors[i].wage);
		app.setMoneyBalanceLabel();
		doctors[i]=null;
		tableModel.setValueAt(null, i, 0);
		tableModel.setValueAt(null, i, 1);
		tableModel.setValueAt(null, i, 2);
		tableModel.setValueAt(null, i, 3);
	}
		
}
