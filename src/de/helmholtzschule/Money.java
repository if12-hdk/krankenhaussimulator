package de.helmholtzschule;

import java.io.Serializable;

public class Money implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//--> attributes
	
	double credit;
	double balanceSheet;
	
	//--> constructor
	
	Money(){
		this.credit = 20000;
		this.balanceSheet = -1000;
	}
	
	//--> methods
	
	public double getCredit() {
		return this.credit;
	}
	
	public double getBalanceSheet() {
		return this.balanceSheet;
	}
	
	public void setBalanceSheet(double value) {
		this.balanceSheet = getBalanceSheet() + value;
		//this.moneyBalance.setText("Balance: "+Double.toString(getBalanceSheet()));
	}
	
	public void setCredit(double value) {
		this.credit = getCredit() + value;
		//this.moneyAmmount.setText("Credits: "+Double.toString(getCredit()));
	}
	
	public void setDailyCharge() {
		this.setCredit(getBalanceSheet());
	}
}
